require 'set'

class Hangman
  attr_accessor :player, :board_word

  def initialize (mode = 1)
    @ai = ComputerPlayer.new("2of12inf.txt")
    @human = HumanPlayer.new
    @letters_left = ('a'..'z').to_a
    @player, @guesser = (mode.to_i == 1) ? [@ai, @human]: [@human, @ai]
  end

  def set_word_length(player)
    @board_word = ['_'] * @player.choose_word_length
  end

  def play
    set_word_length(player)
    misses_left = 6

    while true
      puts "You have #{misses_left} misses left."

      begin
        guessed_letter = @guesser.guess(@letters_left, board_word)
      rescue ArgumentError => error
        puts "Yeah, no."
        puts error.message
      end


        @letters_left.delete(guessed_letter)



      if player.check?(guessed_letter)
        player.update_word(guessed_letter, board_word)
      else
        misses_left -= 1
      end

      break unless board_word.include?("_")
      break if misses_left <= 0
    end

    board_word.include?("_") ? (puts "You Suck!") : (puts "You are Pro!")
    puts "#{board_word}"
  end
end

class ComputerPlayer
  attr_accessor :dictionary

  def initialize(dictionary_words)
    @dictionary = Set.new
    File.foreach(dictionary_words) do |line|
      @dictionary.add(line.delete('%').strip)
    end
  end

  def choose_word_length
    @chosen_word = @dictionary.to_a.sample
    @chosen_word.length
  end

  def modify_dictionary(board_word)
    dictionary.select! { |word| word.length == board_word.length}

    #puts dictionary ???
    test_string = board_word.to_a.join("").gsub("_",".")
    dictionary.select! { |word| (/#{test_string}/ =~ word) == 0 }
    dictionary
  end

  def guess(letters_left, board_word)
    puts "Word on Board: #{board_word}"
    modify_dictionary(board_word)
    optimal_letter_finder(letters_left)
  end


  def optimal_letter_finder(letters_left)
    all_words_string = dictionary.to_a.join("")
    optimal_letter = ""
    optimal_letter_count = 0
    letters_left.each do |letter|
      if all_words_string.count(letter) > optimal_letter_count
        optimal_letter = letter
        optimal_letter_count = all_words_string.count(letter)
      end
    end

    optimal_letter
  end

  def check?(guessed_letter)
    @chosen_word.include?(guessed_letter)
  end

  def update_word(guessed_letter, board_word)
    board_word.each_index do |index|
      board_word[index] = guessed_letter if guessed_letter == @chosen_word[index]
    end

    board_word
  end
end

class HumanPlayer

  def choose_word_length
    puts "Choose the length of the word for the computer to go nuts on."
    length = gets.strip.to_i
  end

  def guess(letters_left, board_word)
    puts "Word on Board: #{board_word}"
    puts "Guess a letter: "
    letter = gets.strip

    unless letter.ord.between?(65, 90) || letter.ord.between?(97,122)
      raise ArgumentError.new "That's not a letter, tough guy. "
    end

  end

  def check?(guessed_letter)
    puts "Does your word include #{guessed_letter} (y/n)?"
    confirmation = gets.strip
    confirmation.downcase == 'y' ? true : false
  end

  def update_word(guessed_letter, board_word)
    puts "Computer guessed #{guessed_letter}. Please update."
    puts "#{board_word}"
    puts "Please enter indices(starting from 0) where #{guessed_letter} occurs. (separated by spaces)"
    indices = gets.strip.split
    indices.each { |index| board_word[index.to_i] = guessed_letter}
  end
end

