class Employee
  attr_accessor :name, :title, :salary, :boss

  def initialize(name)
    @name = name
  end

  def calculate_bonus(multiplier)
    @bonus = @salary * multiplier
  end
end


class Manager < Employee
  attr_reader :employees

  def initialize(name, employees = [])
    super(name)
    @employees = employees
  end

  def employ(applicant)
    @employees << applicant
  end

  def calculate_bonus(multiplier)
    employees.inject(0) { |total_salary,employee| total_salary += employee.salary
    }
  end

end

bob = Employee.new("bob")
steve = Employee.new("steve")
jane = Employee.new("jane")
anaxamander = Employee.new("anaxamander")
[bob, steve, jane, anaxamander].each {|person| person.salary = 10}
bob.calculate_bonus(2)
richard = Manager.new("richard", [bob, steve, jane, anaxamander])