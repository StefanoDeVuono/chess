#!/usr/bin/ruby

# 6 types of pieces
# piece parent class
# - just has position
# - team
#
#
# each subclass has its own move method
# - move_valid? based on path and position
#
# board interacts with all pieces
# - knows where all the pieces are
# - czech for czech

# require the shit out of debugger
require 'debugger'

class Board
  attr_accessor :state

  def initialize
    @state = Array.new(8) {Array.new(8) {:_}}
    #@state[1].map {|square| square = Pawn.new()}
    #place pieces for white/black

    [:white, :black].each do |colour|
      place_pawns(colour)
      place_rooks(colour)
      place_knights(colour)
      place_bishops(colour)
      place_queens(colour)
      place_kings
    end

  end

  def place_pawns(colour)
    row = 1
    row = 6 if colour == :white
    counter = 0
    @state[row].map! do |square|
      square = Pawn.new([row, counter], colour)
      counter += 1
      square
    end
  end

  def place_rooks(colour)
    positions = (colour == :black) ? [[0,0], [0,7]] : [[7,0], [7,7]]
    positions.each {|position| @state[position.first][position.last] = Rook.new(position, colour) }
  end

  def place_knights(colour)
    positions = (colour == :black) ? [[0,1], [0,6]] : [[7,1], [7,6]]
    positions.each {|position| @state[position.first][position.last] = Knight.new(position, colour) }
  end

  def place_bishops(colour)
    positions = (colour == :black) ? [[0,2], [0,5]] : [[7,2], [7,5]]
    positions.each {|position| @state[position.first][position.last] = Bishop.new(position, colour) }
  end

  def place_queens(colour)
    positions = (colour == :black) ? [[0,3]] : [[7,3]]
    positions.each {|position| @state[position.first][position.last] = Queen.new(position, colour) }
  end

  def place_kings
    @black_king = King.new [0,4], :black
    @white_king = King.new [7,4], :white
    [@black_king, @white_king].each {|king| @state[king.position.first][king.position.last] = king }
  end

  def display
    row_number = 8
    board = @state
    board.each do |row|
      print "#{row_number}|  #{row.join('   ')}\n"
      print " |\n" if row_number > 1
      row_number -= 1
    end
    puts "  --------------------------------"
    puts "    #{('a'..'h').to_a.join('   ')}"
  end

  def change_state
    input_move = gets.chomp.split
    start = input_move.first.split('')
    destination = input_move.last.split('')

    start, destination = [start, destination].map! do |position|
       [8 - position.last.to_i, position.first.downcase.ord - 97]
    end

    curr_piece = @state[start.first][start.last]
    destination_piece = @state[destination.first][destination.last]
    destination_team = destination_piece.is_a?(Piece) ? destination_piece.team : false

    if curr_piece.move_valid?(destination, @state)  && curr_piece.team != destination_team
       curr_piece.move(@state, destination)
     else
       "Nice try! You are a charming and wonderful person, but you suck at Chess."
     end
  end

  # def check?(king)
#     # get king position
#     destination = king.position
#     king_colour = king.team
#
#     @state.each do |row|
#       row.each do |space|
#         if space.is_a? Piece
#           space.team
#           # get pieces possible moves towards king
#         end
#       end
#     end
#
#
#   end


end

class Piece
  attr_accessor :position, :team, :movement_deltas

  def initialize(start_position, team, movement_deltas = [])
    @team = team
    @position = start_position
    @movement_deltas = movement_deltas
  end

  def move(board_state, destination)
    # To be filled individually.
    board_state[@position.first][@position.last] = :_
    @position = destination
    board_state[@position.first][@position.last] = self
  end

  def move_valid?(destination, board_state)
    #debugger
    valid_moves = @movement_deltas.map {|delta| add_arrays([delta, @position])}
    validity = valid_moves.include? destination
    p validity
    return true if validity
    p valid_moves

    if [Bishop, Rook, Queen].include? self.class
      8.times do |num|

        validity = valid_moves.include? destination
        p validity
        return true if validity

        valid_moves.map! do |curr_move|
          curr_move = space_free?(curr_move, board_state) ? curr_move : [false,false]
        end

        valid_moves = @movement_deltas.map { |delta| add_arrays([delta, valid_moves.shift]) }

        validity = valid_moves.include? destination
        return true if validity
      end
    end
    false

  end

  def add_arrays(arr)
    # takes a big meta-array, adds the contents of the sub-arrays
    arr.transpose.map do |x|
      if x.include? false
        return [false,false]
      else
        x.reduce(:+)
      end
    end
  end


  def space_free? (position, board_state)
    return false if position == [false, false]
    if position.first.between?(0,7) && position.last.between?(0,7)

      square_value = board_state[position.first][position.last]

      return true if square_value == :_
    end
    false
  end


  def inspect
    return '*' if self.is_a? King
    return self.class.to_s[0]
  end

  def to_s
    return '*' if self.is_a? King
    return self.class.to_s[0]
  end

end

class Pawn < Piece
  def initialize(start_position, team)
    super(start_position, team)
    @movement_deltas = [
      [-1, -1],
      [-1, 0],
      [-1, 1]
    ] if team == :white
    @movement_deltas = [
      [1, -1],
      [1, 0],
      [1, 1]
    ] if team == :black
  end
end


class Rook < Piece
  def initialize(start_position, team)
    super(start_position, team)
    @movement_deltas = [
      [-1,  0],
      [ 1,  0],
      [ 0,  1],
      [ 0, -1]
    ]
  end
end

class Knight < Piece
  def initialize(start_position, team)
    super(start_position, team)
    @movement_deltas = [
      [-2, 1],
      [-2, -1],
      [2, 1],
      [2, -1],
      [-1, 2],
      [-1, -2],
      [1, 2],
      [1, -2]
    ]
  end
end

class Bishop < Piece
  def initialize(start_position, team)
    super(start_position, team)
    @movement_deltas = [
      [-1,  1],
      [ 1,  1],
      [-1, -1],
      [ 1, -1]
    ]
  end
end

class Queen < Piece
  def initialize(start_position, team)
    super(start_position, team)
    @movement_deltas = [
      [-1,  1],
      [ 1,  1],
      [-1, -1],
      [ 1, -1],
      [-1,  0],
      [ 1,  0],
      [ 0,  1],
      [ 0, -1]
    ]
  end
end

class King < Piece

  attr_reader :check_deltas_q, :check_deltas_k

  def initialize(start_position, team)
    super(start_position, team)
    @movement_deltas = [
      [-1,  1],
      [ 1,  1],
      [-1, -1],
      [ 1, -1],
      [-1,  0],
      [ 1,  0],
      [ 0,  1],
      [ 0, -1]
    ]
    @check_deltas_q = [
      [-1,  1],
      [ 1,  1],
      [-1, -1],
      [ 1, -1],
      [-1,  0],
      [ 1,  0],
      [ 0,  1],
      [ 0, -1]
    ]
    @check_deltas_k = [
      [-2, 1],
      [-2, -1],
      [2, 1],
      [2, -1],
      [-1, 2],
      [-1, -2],
      [1, 2],
      [1, -2]
    ]
  end
end

if $PROGRAM_NAME == __FILE__
  game = Board.new
  game.display
  game.change_state
end
